/**
 * Created by joshua.fair on 6/7/2016.
 */
export interface Vins {
    modelYear: number;
    make: string;
    model: string;
    trim: string;
    series: string;
    engineType: string;
    fuelType: string;
    bodyType: string;
    doors: number;
    grossWgt: number;
    vehicleType: string;
    ncicMake: string;
    hybridInd: number;
    ncicModel: string;
    vin: string;
    tnclerkBodyType: string;
    tnclerkFuelType: string;
    tnclerkVehicleType: string;
    tnClerkWeightType: number;
}

export class Vin implements Vins {

    constructor(public modelYear: number,
                public make: string,
                public model: string,
                public trim: string,
                public series: string,
                public engineType: string,
                public fuelType: string,
                public bodyType: string,
                public doors: number,
                public grossWgt: number,
                public vehicleType: string,
                public ncicMake: string,
                public hybridInd: number,
                public ncicModel: string,
                public vin: string,
                public tnclerkBodyType: string,
                public tnclerkFuelType: string,
                public tnclerkVehicleType: string,
                public tnClerkWeightType: number){}
}