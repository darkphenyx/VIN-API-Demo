/**
 * Created by joshua.fair on 6/8/2016.
 */
import { Injectable } from 'angular2/core';
import { Http, Response } from 'angular2/http';
import { Observable } from 'rxjs/Observable';

import {Vins} from './vins';

@Injectable()
export class VinService {
    private _vinUrl = 'https://api.bisonline.com/vin-ppa/decode/';

    constructor(private _http: Http){}

    getVehicles(): Observable<Vins[]> {
        return this._http.get(this._vinUrl)
            .map((response: Response) => <Vins[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    getVehicle(id: string): Observable<Vins> {
        var _newUrl = this._vinUrl + id;
        console.log(_newUrl);
        return this._http.get(_newUrl)
            .map((response: Response) => <Vins[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}