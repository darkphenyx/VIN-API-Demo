/**
 * Created by joshua.fair on 6/7/2016.
 */
import { Component, OnInit } from 'angular2/core';
import {ROUTER_DIRECTIVES} from "angular2/router";

import {VinService} from "./vin.service";
import {Vins} from "./vins";


@Component({
    templateUrl: 'app/vin/vin-enter.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class VinComponent implements OnInit{
    pageTitle: string = "Please enter the VIN you wish to search";
    listFilter: string = '';
    errorMessage: string;
    vin: Vins[];

    constructor(private _vinService: VinService) {

    }

    ngOnInit(): void {
        var vin;
        this._vinService.getVehicles()
            .subscribe(
                products => this.vin = vin,
                error =>  this.errorMessage = <any>error);
    }

}