/**
 * Created by joshua.fair on 6/8/2016.
 */
import { Component, OnInit } from 'angular2/core';
import { RouteParams, Router } from 'angular2/router';

import {Vins} from './vins';
import {VinService} from './vin.service';

@Component({
    templateUrl: 'app/vin/vin-detail.component.html',
    styleUrls: ['app/vin/vin-detail.component.css']
})

export class VinDetailComponent implements OnInit {
    pageTitle: string = 'Vehicle Details';
    vin: Vins;
    errorMessage: string;

    constructor(private _vinService: VinService,
                private _router: Router,
                private _routeParams: RouteParams) {

    }

    ngOnInit(){
        if(!this.vin){
            let id = this._routeParams.get('id');
            this.getVehicle(<string>id);
        }
    }

    getVehicle(id: string){
        this._vinService.getVehicle(id)
            .subscribe(
                product => this.vin = product,
                error => this.errorMessage = <any>error);
    }

    onBack(): void {
        this._router.navigate(['VIN']);
    }
}