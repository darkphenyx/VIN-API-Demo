/**
 * Created by joshua.fair on 6/7/2016.
 */
import { Component } from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';
import 'rxjs/Rx'; //Load all features
import {ROUTER_PROVIDERS, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {WelcomeComponent} from "./home/welcome.component";
import {VinComponent} from "./vin/vin-enter.component";
import {VinService} from './vin/vin.service';
import {VinDetailComponent} from './vin/vin-detail.component';


@Component({
    selector: 'pm-app',
    template:`
    <div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a class="navbar-brand">{{pageTitle}}</a>
                <ul class="nav navbar-nav">
                    <li><a [routerLink]="['Welcome']">Home</a></li>
                    <li><a [routerLink]="['VIN']">VIN Lookup</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <router-outlet></router-outlet>
        </div>
    </div>
`,
    directives: [ROUTER_DIRECTIVES],
    providers: [VinService,
                HTTP_PROVIDERS,
                ROUTER_PROVIDERS]
})
@RouteConfig([
    { path: '/welcome', name: 'Welcome', component: WelcomeComponent, useAsDefault: true},
    { path: '/vin', name: 'VIN', component: VinComponent},
    { path: '/vins/:id', name: 'VinDetail', component: VinDetailComponent }
])
export class AppComponent{
    pageTitle: string = 'VIN Decoder';
}