/**
 * Created by joshua.fair on 6/7/2016.
 */
import { bootstrap } from 'angular2/platform/browser';

// Our main component
import { AppComponent } from './app.component';

bootstrap(AppComponent);